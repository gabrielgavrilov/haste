from nodes import *

class Interpreter:
	def __init__(self):
		pass

	def interpret(self, node):
		if isinstance(node, PrintNode):
			self.interpret_print(node)

	def interpret_print(self, node):
		data = node.value
		print(data.value)