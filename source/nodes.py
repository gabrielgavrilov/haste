class NumberNode:
	def __init__(self, value):
		self.value = int(value)

	def __repr__(self):
		return f"(NUMBER ({self.value}))"

class AddNode:
	def __init__(self, node_a, node_b):
		self.node_a = node_a
		self.node_b = node_b
		
		if isinstance(node_a.value, int) and isinstance(node_b.value, int):
			self.value = int(node_a.value) + int(node_b.value)

		else:
			self.value = str(node_a.value) + str(node_b.value)

	def __repr__(self):
		return f"(ADD {self.node_a}, {self.node_b})"

class SubtractNode:
	def __init__(self, node_a, node_b):
		self.node_a = node_a
		self.node_b = node_b
		self.value = int(node_a.value) - int(node_b.value)

	def __repr__(self):
		return f"(SUBTRACT {self.node_a}, {self.node_b})"

class StringNode:
	def __init__(self, value):
		self.value = value

	def __repr__(self):
		return f"(STRING ({self.value}))"

class PrintNode:
	def __init__(self, value):
		self.value = value
	
	def __repr__(self):
		return f"(PRINT {self.value})"

class VariableNode:
	def __init__(self, name, value):
		self.name = name
		self.value = value
	
	def __repr__(self):
		return f"(VARIABLE ({self.name}) ::= ({self.value}))"