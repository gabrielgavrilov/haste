import sys
from tokens import *
from lexer import *
from parser import *

def main():
	
	source = open(sys.argv[1], "r")
	lexer = Lexer(source.read())
	parser = Parser(lexer)

	parser.program()

if __name__ == "__main__":
	main()