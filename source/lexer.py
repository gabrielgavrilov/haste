from tokens import *

class Lexer:
	def __init__(self, input):
		self.source = input
		self.source_size = len(self.source)
		self.current_char = ''
		self.current_position = -1;

		self.advance_character()

	def advance_character(self):
		self.current_position+=1

		if self.current_position >= self.source_size:
			self.current_char = '\0'

		else:
			self.current_char = self.source[self.current_position]

	def peek(self):
		if self.current_position + 1 >= self.source_size:
			return '\0'

		return self.source[self.current_position + 1]

	def skip_whitespace(self):
		while self.current_char == ' ' or self.current_char == '\t':
			self.advance_character()

	def generate_token(self):
		token = None
		self.skip_whitespace()

		if self.current_char == '\0':
			token = Token(TokenType.EOF, '')

		elif self.current_char == '(':
			token = Token(TokenType.LEFT_PAREN, self.current_char)

		elif self.current_char == ')':
			token = Token(TokenType.RIGHT_PAREN, self.current_char)

		elif self.current_char == '=':
			token = Token(TokenType.EQUALS, self.current_char)

		elif self.current_char == '+':
			token = Token(TokenType.PLUS, self.current_char)

		elif self.current_char == '-':
			token = Token(TokenType.MINUS, self.current_char)

		elif self.current_char == '*':
			token = Token(TokenType.MULTIPLY, self.current_char)

		elif self.current_char == '/':
			token = Token(TokenType.DIVIDE, self.current_char)

		elif self.current_char == '\"':
			string = ""
			self.advance_character()
			while self.current_char != '\"':
				string += self.current_char
				self.advance_character()

			token = Token(TokenType.STRING, string)

		elif self.current_char.isdigit():
			start_pos = self.current_position
			while self.peek().isdigit():
				self.advance_character()

			number = self.source[start_pos : self.current_position + 1]
			token = Token(TokenType.NUMBER, number)

		elif self.current_char.isalpha():
			start_pos = self.current_position
			while self.peek().isalnum():
				self.advance_character()

			value = self.source[start_pos : self.current_position + 1]
			keyword = self.check_keyword(value)

			token = Token(keyword, value)

		elif self.current_char == '\n':
			token = Token(TokenType.NEWLINE, self.current_char)

		self.advance_character()
		return token

	def check_keyword(self, value):
		for token in TokenType:
			if token.name.lower() == value:
				return token

		return TokenType.IDENTIFIER