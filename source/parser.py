import sys
from tokens import *
from nodes import *
from interpreter import *

class Parser:
	# initializes the parser
	def __init__(self, lexer):
		self.lexer = lexer

		self.variables = set()
		
		self.current_token = None
		self.peek_token = None
		self.advance()
		self.advance()

	# returns a boolean to which if the given type matches with the current token's type
	def check(self, type):
		return type == self.current_token.type

	# returns a boolean to which if the given type matches with the peek token's type
	def check_peek(self, type):
		return type == self.peek_token.type

	# if token type does not match with the given type, this will throw an error
	def match(self, type):
		if not self.check(type):
			self.abort(f"Expected {type}, got {self.current_token.type}")

	# advances the parser
	def advance(self):
		self.current_token = self.peek_token
		self.peek_token = self.lexer.generate_token()

	def abort(self, message):
		sys.exit(f"[Parsing error]: {message}")

	def program(self):
		interpreter = Interpreter()
		while self.check(TokenType.NEWLINE):
			self.advance()

		while not self.check(TokenType.EOF):
			result = self.statement()
			interpreter.interpret(result)

	def statement(self):
		result = self.comparison()

		if self.current_token.type == TokenType.PRINT:
			self.advance()
			result = PrintNode(self.comparison())

		if self.current_token.type == TokenType.IDENTIFIER:
			if self.check_peek(TokenType.EQUALS):
				identifier_name = self.current_token.value
				self.advance()
				result = VariableNode(identifier_name, self.comparison())
				self.variables.add(result)	

		self.newline()
		return result
	
	def comparison(self):
		result = self.expression()
		
		if self.current_token.type == TokenType.EQUALS:
			self.advance()
			result = self.expression()

		return result
	
	def expression(self):
		result = self.factor()

		if self.current_token.type == TokenType.PLUS:
			self.advance()
			result = AddNode(result, self.factor())

		if self.current_token.type == TokenType.MINUS:
			self.advance()
			result = SubtractNode(result, self.factor())

		return result

	def factor(self):
		token = self.current_token

		if token.type == TokenType.LEFT_PAREN:
			self.advance()
			result = self.statement()

			if self.current_token.type != TokenType.RIGHT_PAREN:
				self.raise_error()
			
			self.advance()
			return result

		if token.type == TokenType.STRING:
			self.advance()
			return StringNode(token.value)

		if token.type == TokenType.NUMBER:
			self.advance()
			return NumberNode(token.value)
		
		if token.type == TokenType.IDENTIFIER:
			for variable in self.variables:
				if variable.name == token.value:
					self.advance()
					return variable.value

	def newline(self):
		while self.check(TokenType.NEWLINE):
			self.advance()